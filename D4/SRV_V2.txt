{
  "actors": [
    {
      "id": "5030a35d-c52a-47b5-b14e-dcbc2177f711",
      "text": "Espetador",
      "type": "istar.Role",
      "x": 727,
      "y": 601,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "51fada58-4736-4f5a-a6ca-095f556ab8fa",
          "text": "Bilhete Comprado",
          "type": "istar.Goal",
          "x": 845,
          "y": 685,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "d369d8e2-3c89-4554-8e60-d8d9438de444",
          "text": "Espetáculo encontrado",
          "type": "istar.Goal",
          "x": 732,
          "y": 757,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "fd2f1395-c7b2-46ac-bab1-bc97d836340e",
          "text": "Pagamento autorizado",
          "type": "istar.Goal",
          "x": 953,
          "y": 757,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "042685a6-ba2c-476e-952d-71a2fd54b690",
          "text": "Procurar o espetáculo",
          "type": "istar.Task",
          "x": 727,
          "y": 825,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "4b0756eb-9f2e-415f-8d29-837a57051266",
          "text": "Lugar escolhido",
          "type": "istar.Goal",
          "x": 844,
          "y": 755,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "1b136978-ae63-4787-aae3-6b2bfcc0e7a9",
          "text": "Efetuar o pagamento",
          "type": "istar.Task",
          "x": 949,
          "y": 821,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "7f245c34-c3b4-41e9-b55d-dbf881b5e312",
          "text": "Ver fatura no email",
          "type": "istar.Task",
          "x": 1065,
          "y": 830,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "7f791761-499d-4f99-8550-51b2bcf9bb66",
          "text": "Confirmação recebida",
          "type": "istar.Goal",
          "x": 1064,
          "y": 756,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "d0985a63-7c69-4330-88d4-ba7747ba2cab",
          "text": "Ver bilhetes comprados",
          "type": "istar.Goal",
          "x": 995,
          "y": 687,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "2200a110-1feb-4367-904c-b220da4044ab",
          "text": "Dúvidas esclarecidas",
          "type": "istar.Goal",
          "x": 844,
          "y": 605,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "17e057ab-352a-45fe-ad91-ae5da1dfd95d",
          "text": "Contactar o suporte",
          "type": "istar.Task",
          "x": 996,
          "y": 601,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "ef17494c-c2ac-4443-9ec3-f1f027854bf6",
      "text": "Gestor da Sala de Espetáculos",
      "type": "istar.Role",
      "x": 83,
      "y": 560,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "d5790832-0037-4a17-bd01-cb25fe03b351",
          "text": "Evento criado",
          "type": "istar.Goal",
          "x": 367,
          "y": 566,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "28443267-01d0-4083-a3e8-d2af429ea0e7",
          "text": "Sala configurada",
          "type": "istar.Goal",
          "x": 265,
          "y": 638,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "f5fa1a55-ef44-4492-b4bb-84ef92a7eedb",
          "text": "Tipo de bilhetes e preço definidos",
          "type": "istar.Goal",
          "x": 361,
          "y": 634,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "77b2280f-3466-4a77-af12-95caa3e3b0a8",
          "text": "Informações do evento definidas",
          "type": "istar.Goal",
          "x": 468,
          "y": 635,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "c3df1858-7d39-4493-9b1f-1c111b9e7d32",
          "text": "Bilhetes vendidos consultados",
          "type": "istar.Goal",
          "x": 156,
          "y": 633,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "14b68359-3340-4a94-8002-d71057842b8f",
          "text": "Estatísticas consultadas",
          "type": "istar.Goal",
          "x": 154,
          "y": 572,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "568ffae5-c634-4d21-9651-f7e1862bdf5a",
          "text": "Estatísticas gerais consultadas",
          "type": "istar.Goal",
          "x": 194,
          "y": 727,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "890383d8-6d81-4210-b5e5-b336b6e80df2",
          "text": "Média de lotação mensal vista",
          "type": "istar.Goal",
          "x": 92,
          "y": 816,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "13d66f7a-95b1-42ac-b75e-cd5b59b2254e",
          "text": "Histórico de últimos espetáculos consultado",
          "type": "istar.Goal",
          "x": 192,
          "y": 810,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "cd39c513-36dc-4105-9474-dfbd5827381a",
          "text": "Receitas mensais consultadas",
          "type": "istar.Goal",
          "x": 294,
          "y": 815,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "765d9a1c-8837-4c0a-9f73-802d7f042a60",
          "text": "Lugares reservados geridos",
          "type": "istar.Goal",
          "x": 373,
          "y": 724,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "f3167f0b-1c4e-4e6b-9190-cedd9974f584",
          "text": "Lucros do evento recebidos",
          "type": "istar.Goal",
          "x": 479,
          "y": 724,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "4c003ef4-85d8-42df-95f3-5a952c0667c8",
      "text": "Entidade Bancária",
      "type": "istar.Agent",
      "x": 977,
      "y": 1008,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "caf6a775-c3d3-4bd3-986b-af2292821f6e",
      "text": "Servidor de Email",
      "type": "istar.Agent",
      "x": 1097,
      "y": 1010,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "46ac7cfe-5aba-4915-b9c4-abe32b081952",
      "text": "Sala-Z",
      "type": "istar.Agent",
      "x": 764,
      "y": 51,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "391109cb-520c-4085-a2d5-ded8426337b4",
          "text": "Website user-friendly",
          "type": "istar.Goal",
          "x": 1103,
          "y": 168,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "ee3a9136-0c41-40f6-99a6-df1c3e3085d5",
          "text": "Acessos à base de dados otimizados",
          "type": "istar.Goal",
          "x": 1100,
          "y": 223,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "42fe47ea-210d-4666-a032-f1e49c944c44",
          "text": "Pagamentos automáticos",
          "type": "istar.Goal",
          "x": 806,
          "y": 200,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "31fd0199-0c96-44e2-bb84-1dd5ddaf89b8",
          "text": "Usar trigger para enviar lucros ao gestor da sala de espetáculo automaticamente",
          "type": "istar.Task",
          "x": 793,
          "y": 270,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "daab9595-e15f-4fa2-a140-86172debf3be",
          "text": "Encriptar dados",
          "type": "istar.Task",
          "x": 1004,
          "y": 70,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "dcf104ed-4014-4dde-85a4-baad3890ad9e",
          "text": "Segurança do utilizador",
          "type": "istar.Quality",
          "x": 870,
          "y": 58,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "5b8565b9-3e04-4ddf-83be-f8272d4759fe",
          "text": "Segurança do sistema",
          "type": "istar.Quality",
          "x": 1008,
          "y": 135,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "46117aaa-f8f0-4a31-a308-c07d50372605",
          "text": "Dar suporte",
          "type": "istar.Quality",
          "x": 1104,
          "y": 286,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "20c95a3d-78e5-4772-acf9-1775dfa4b9fb",
      "text": "Utilizador do Sala-Z",
      "type": "istar.Role",
      "x": 534,
      "y": 399,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    }
  ],
  "orphans": [],
  "dependencies": [
    {
      "id": "a7a70789-08f3-4bf1-86df-42e0ce587c3b",
      "text": "Confirmar pagamento",
      "type": "istar.Goal",
      "x": 951,
      "y": 907,
      "customProperties": {
        "Description": ""
      },
      "source": "1b136978-ae63-4787-aae3-6b2bfcc0e7a9",
      "target": "4c003ef4-85d8-42df-95f3-5a952c0667c8"
    },
    {
      "id": "f600af98-537a-4afd-a503-3081244584b7",
      "text": "Receber fatura",
      "type": "istar.Goal",
      "x": 1066,
      "y": 908,
      "customProperties": {
        "Description": ""
      },
      "source": "7f245c34-c3b4-41e9-b55d-dbf881b5e312",
      "target": "caf6a775-c3d3-4bd3-986b-af2292821f6e"
    },
    {
      "id": "75a0cb12-cc48-4772-a8dc-1a7a68d0e748",
      "text": "Evento",
      "type": "istar.Resource",
      "x": 532,
      "y": 500,
      "customProperties": {
        "Description": ""
      },
      "source": "042685a6-ba2c-476e-952d-71a2fd54b690",
      "target": "d5790832-0037-4a17-bd01-cb25fe03b351"
    },
    {
      "id": "bdc9b700-7df8-48c8-bca5-5a521b5882f5",
      "text": "Receber lucros",
      "type": "istar.Goal",
      "x": 709,
      "y": 455,
      "customProperties": {
        "Description": ""
      },
      "source": "f3167f0b-1c4e-4e6b-9190-cedd9974f584",
      "target": "31fd0199-0c96-44e2-bb84-1dd5ddaf89b8"
    },
    {
      "id": "286049bc-e673-4f98-b6a2-606eabcf7bac",
      "text": "Criar eventos",
      "type": "istar.Goal",
      "x": 426,
      "y": 329,
      "customProperties": {
        "Description": ""
      },
      "source": "46ac7cfe-5aba-4915-b9c4-abe32b081952",
      "target": "ef17494c-c2ac-4443-9ec3-f1f027854bf6"
    },
    {
      "id": "45b6fd52-ae03-4023-b2d8-db8b3dd91629",
      "text": "Fácil criar um evento",
      "type": "istar.Quality",
      "x": 336,
      "y": 266,
      "customProperties": {
        "Description": ""
      },
      "source": "ef17494c-c2ac-4443-9ec3-f1f027854bf6",
      "target": "46ac7cfe-5aba-4915-b9c4-abe32b081952"
    },
    {
      "id": "331bcb39-5ea0-4ce9-8134-28acab80773b",
      "text": "Fácil encontrar um espetáculo",
      "type": "istar.Quality",
      "x": 983,
      "y": 393,
      "customProperties": {
        "Description": ""
      },
      "source": "5030a35d-c52a-47b5-b14e-dcbc2177f711",
      "target": "46ac7cfe-5aba-4915-b9c4-abe32b081952"
    },
    {
      "id": "9b6e6774-e8ec-4ac4-9713-6d15fa304209",
      "text": "Comprar bilhetes",
      "type": "istar.Goal",
      "x": 877,
      "y": 399,
      "customProperties": {
        "Description": ""
      },
      "source": "5030a35d-c52a-47b5-b14e-dcbc2177f711",
      "target": "46ac7cfe-5aba-4915-b9c4-abe32b081952"
    },
    {
      "id": "a77953cf-5627-4d49-843f-145d62ce1cac",
      "text": "Segurança garantida",
      "type": "istar.Goal",
      "x": 668,
      "y": 256,
      "customProperties": {
        "Description": ""
      },
      "source": "20c95a3d-78e5-4772-acf9-1775dfa4b9fb",
      "target": "dcf104ed-4014-4dde-85a4-baad3890ad9e"
    },
    {
      "id": "59c1e236-cff5-44e3-b52b-a8a0e396938b",
      "text": "Receber suporte",
      "type": "istar.Goal",
      "x": 1103,
      "y": 402,
      "customProperties": {
        "Description": ""
      },
      "source": "5030a35d-c52a-47b5-b14e-dcbc2177f711",
      "target": "46117aaa-f8f0-4a31-a308-c07d50372605"
    }
  ],
  "links": [
    {
      "id": "a27e7aa7-55aa-49bc-a7aa-2f95b679613c",
      "type": "istar.DependencyLink",
      "source": "59c1e236-cff5-44e3-b52b-a8a0e396938b",
      "target": "46117aaa-f8f0-4a31-a308-c07d50372605"
    },
    {
      "id": "9d2b7294-d011-44bd-8f05-00b2a2f5ea9c",
      "type": "istar.DependencyLink",
      "source": "5030a35d-c52a-47b5-b14e-dcbc2177f711",
      "target": "59c1e236-cff5-44e3-b52b-a8a0e396938b"
    },
    {
      "id": "886b076a-e567-4e04-a3ba-0f3b07c16d13",
      "type": "istar.DependencyLink",
      "source": "a77953cf-5627-4d49-843f-145d62ce1cac",
      "target": "dcf104ed-4014-4dde-85a4-baad3890ad9e"
    },
    {
      "id": "95a9b67d-32e6-45ce-b47d-3c4cc342ba69",
      "type": "istar.DependencyLink",
      "source": "20c95a3d-78e5-4772-acf9-1775dfa4b9fb",
      "target": "a77953cf-5627-4d49-843f-145d62ce1cac"
    },
    {
      "id": "d68a95b2-c33b-421d-a321-ed104c15fb82",
      "type": "istar.DependencyLink",
      "source": "9b6e6774-e8ec-4ac4-9713-6d15fa304209",
      "target": "46ac7cfe-5aba-4915-b9c4-abe32b081952"
    },
    {
      "id": "778768c7-3a3a-4587-b441-7af7fdbdbe7c",
      "type": "istar.DependencyLink",
      "source": "5030a35d-c52a-47b5-b14e-dcbc2177f711",
      "target": "9b6e6774-e8ec-4ac4-9713-6d15fa304209"
    },
    {
      "id": "08caaa35-7d5d-4e5a-a078-3ae1e5849066",
      "type": "istar.DependencyLink",
      "source": "331bcb39-5ea0-4ce9-8134-28acab80773b",
      "target": "46ac7cfe-5aba-4915-b9c4-abe32b081952"
    },
    {
      "id": "603fa46a-6d3d-47d9-bb99-ddd533a07571",
      "type": "istar.DependencyLink",
      "source": "5030a35d-c52a-47b5-b14e-dcbc2177f711",
      "target": "331bcb39-5ea0-4ce9-8134-28acab80773b"
    },
    {
      "id": "50c30ec6-1d18-469a-9a2a-298da2c2b722",
      "type": "istar.DependencyLink",
      "source": "45b6fd52-ae03-4023-b2d8-db8b3dd91629",
      "target": "46ac7cfe-5aba-4915-b9c4-abe32b081952"
    },
    {
      "id": "9ff48cc1-5195-4906-b162-67cfa8d34982",
      "type": "istar.DependencyLink",
      "source": "ef17494c-c2ac-4443-9ec3-f1f027854bf6",
      "target": "45b6fd52-ae03-4023-b2d8-db8b3dd91629"
    },
    {
      "id": "32295a9a-84ec-4031-bdd8-b114c49cbfed",
      "type": "istar.DependencyLink",
      "source": "286049bc-e673-4f98-b6a2-606eabcf7bac",
      "target": "ef17494c-c2ac-4443-9ec3-f1f027854bf6"
    },
    {
      "id": "d806f1e6-9b0b-4ea2-81d7-41ac2b1f2449",
      "type": "istar.DependencyLink",
      "source": "46ac7cfe-5aba-4915-b9c4-abe32b081952",
      "target": "286049bc-e673-4f98-b6a2-606eabcf7bac"
    },
    {
      "id": "fd773539-936c-4334-ac7d-77156b758974",
      "type": "istar.DependencyLink",
      "source": "bdc9b700-7df8-48c8-bca5-5a521b5882f5",
      "target": "31fd0199-0c96-44e2-bb84-1dd5ddaf89b8"
    },
    {
      "id": "7e5e2a41-78ec-4b0d-93c2-ffcced33cf4e",
      "type": "istar.DependencyLink",
      "source": "f3167f0b-1c4e-4e6b-9190-cedd9974f584",
      "target": "bdc9b700-7df8-48c8-bca5-5a521b5882f5"
    },
    {
      "id": "21c9b5a7-808a-492a-8ac6-861b0e6dbdf1",
      "type": "istar.DependencyLink",
      "source": "75a0cb12-cc48-4772-a8dc-1a7a68d0e748",
      "target": "d5790832-0037-4a17-bd01-cb25fe03b351"
    },
    {
      "id": "28df3037-8cc7-46b8-b96a-ce2f49998d2f",
      "type": "istar.DependencyLink",
      "source": "042685a6-ba2c-476e-952d-71a2fd54b690",
      "target": "75a0cb12-cc48-4772-a8dc-1a7a68d0e748"
    },
    {
      "id": "61178fe3-4d0f-4ebf-87b3-b7dceb55b258",
      "type": "istar.DependencyLink",
      "source": "f600af98-537a-4afd-a503-3081244584b7",
      "target": "caf6a775-c3d3-4bd3-986b-af2292821f6e"
    },
    {
      "id": "85f29c14-8eb6-4501-acad-e761ad29a84a",
      "type": "istar.DependencyLink",
      "source": "7f245c34-c3b4-41e9-b55d-dbf881b5e312",
      "target": "f600af98-537a-4afd-a503-3081244584b7"
    },
    {
      "id": "4d657dc7-73de-4e40-9ffa-efa878ae5e03",
      "type": "istar.DependencyLink",
      "source": "a7a70789-08f3-4bf1-86df-42e0ce587c3b",
      "target": "4c003ef4-85d8-42df-95f3-5a952c0667c8"
    },
    {
      "id": "f38d56b9-9835-45f1-b409-2a1f0196cd53",
      "type": "istar.DependencyLink",
      "source": "1b136978-ae63-4787-aae3-6b2bfcc0e7a9",
      "target": "a7a70789-08f3-4bf1-86df-42e0ce587c3b"
    },
    {
      "id": "6b7e56b4-768c-42de-b6df-b67a86a1d1ba",
      "type": "istar.AndRefinementLink",
      "source": "d369d8e2-3c89-4554-8e60-d8d9438de444",
      "target": "51fada58-4736-4f5a-a6ca-095f556ab8fa"
    },
    {
      "id": "e7b2d18c-9d94-4f68-a52c-428631e8aee7",
      "type": "istar.AndRefinementLink",
      "source": "fd2f1395-c7b2-46ac-bab1-bc97d836340e",
      "target": "51fada58-4736-4f5a-a6ca-095f556ab8fa"
    },
    {
      "id": "5ed370c9-424c-4bf5-93a1-03d8384c1088",
      "type": "istar.OrRefinementLink",
      "source": "042685a6-ba2c-476e-952d-71a2fd54b690",
      "target": "d369d8e2-3c89-4554-8e60-d8d9438de444"
    },
    {
      "id": "5c9c258c-210b-4a6a-9022-80e0863949c7",
      "type": "istar.AndRefinementLink",
      "source": "4b0756eb-9f2e-415f-8d29-837a57051266",
      "target": "51fada58-4736-4f5a-a6ca-095f556ab8fa"
    },
    {
      "id": "a2c55b47-17d2-4152-856c-fa74d2bb26a4",
      "type": "istar.AndRefinementLink",
      "source": "1b136978-ae63-4787-aae3-6b2bfcc0e7a9",
      "target": "fd2f1395-c7b2-46ac-bab1-bc97d836340e"
    },
    {
      "id": "497b2a62-0717-4b3d-95b2-85c817025ca3",
      "type": "istar.AndRefinementLink",
      "source": "7f791761-499d-4f99-8550-51b2bcf9bb66",
      "target": "fd2f1395-c7b2-46ac-bab1-bc97d836340e"
    },
    {
      "id": "1198fb5d-5a18-4828-9865-319017c732bc",
      "type": "istar.OrRefinementLink",
      "source": "7f245c34-c3b4-41e9-b55d-dbf881b5e312",
      "target": "7f791761-499d-4f99-8550-51b2bcf9bb66"
    },
    {
      "id": "dd89526b-78c9-4815-8a90-4effb2590edd",
      "type": "istar.AndRefinementLink",
      "source": "28443267-01d0-4083-a3e8-d2af429ea0e7",
      "target": "d5790832-0037-4a17-bd01-cb25fe03b351"
    },
    {
      "id": "cd4eb612-7671-4e44-9d94-fb04ad6d5c6e",
      "type": "istar.AndRefinementLink",
      "source": "f5fa1a55-ef44-4492-b4bb-84ef92a7eedb",
      "target": "d5790832-0037-4a17-bd01-cb25fe03b351"
    },
    {
      "id": "e54cc28d-44ac-4ae5-bd87-d2902e78f786",
      "type": "istar.AndRefinementLink",
      "source": "77b2280f-3466-4a77-af12-95caa3e3b0a8",
      "target": "d5790832-0037-4a17-bd01-cb25fe03b351"
    },
    {
      "id": "4cdd08c5-9b64-41db-ac11-dfb4408e3b38",
      "type": "istar.OrRefinementLink",
      "source": "c3df1858-7d39-4493-9b1f-1c111b9e7d32",
      "target": "14b68359-3340-4a94-8002-d71057842b8f"
    },
    {
      "id": "b8a45056-0b66-4e06-aba5-53730b6771e8",
      "type": "istar.OrRefinementLink",
      "source": "890383d8-6d81-4210-b5e5-b336b6e80df2",
      "target": "568ffae5-c634-4d21-9651-f7e1862bdf5a"
    },
    {
      "id": "18f20308-5ea3-4c93-a966-98be8fe1e0b9",
      "type": "istar.OrRefinementLink",
      "source": "13d66f7a-95b1-42ac-b75e-cd5b59b2254e",
      "target": "568ffae5-c634-4d21-9651-f7e1862bdf5a"
    },
    {
      "id": "c917fd25-e269-48b1-9567-763a3128eeab",
      "type": "istar.OrRefinementLink",
      "source": "cd39c513-36dc-4105-9474-dfbd5827381a",
      "target": "568ffae5-c634-4d21-9651-f7e1862bdf5a"
    },
    {
      "id": "82f824f1-e16d-4fc8-bbb9-08ad6933045f",
      "type": "istar.IsALink",
      "source": "ef17494c-c2ac-4443-9ec3-f1f027854bf6",
      "target": "20c95a3d-78e5-4772-acf9-1775dfa4b9fb"
    },
    {
      "id": "7847648f-7553-4524-8130-4d0b12b7e0bb",
      "type": "istar.IsALink",
      "source": "5030a35d-c52a-47b5-b14e-dcbc2177f711",
      "target": "20c95a3d-78e5-4772-acf9-1775dfa4b9fb"
    },
    {
      "id": "3b3f9424-e3af-4dde-bcd9-2a6c0c5a3693",
      "type": "istar.ParticipatesInLink",
      "source": "20c95a3d-78e5-4772-acf9-1775dfa4b9fb",
      "target": "46ac7cfe-5aba-4915-b9c4-abe32b081952"
    },
    {
      "id": "f8453df2-4e4a-4546-b0a7-e33c9712b284",
      "type": "istar.OrRefinementLink",
      "source": "31fd0199-0c96-44e2-bb84-1dd5ddaf89b8",
      "target": "42fe47ea-210d-4666-a032-f1e49c944c44"
    },
    {
      "id": "3fc2e29d-bf53-43cb-9ec4-f0fcabc571f2",
      "type": "istar.OrRefinementLink",
      "source": "17e057ab-352a-45fe-ad91-ae5da1dfd95d",
      "target": "2200a110-1feb-4367-904c-b220da4044ab"
    },
    {
      "id": "8726d879-8b0d-41f9-8983-0bd1d53f9fdf",
      "type": "istar.ContributionLink",
      "source": "daab9595-e15f-4fa2-a140-86172debf3be",
      "target": "dcf104ed-4014-4dde-85a4-baad3890ad9e",
      "label": "make"
    },
    {
      "id": "30abddb0-9b14-40a9-99eb-d964c59440fb",
      "type": "istar.ContributionLink",
      "source": "daab9595-e15f-4fa2-a140-86172debf3be",
      "target": "5b8565b9-3e04-4ddf-83be-f8272d4759fe",
      "label": "help"
    }
  ],
  "display": {
    "1b136978-ae63-4787-aae3-6b2bfcc0e7a9": {
      "width": 98.609375,
      "height": 41.84375
    },
    "f5fa1a55-ef44-4492-b4bb-84ef92a7eedb": {
      "width": 101.56594848632812,
      "height": 46.888893127441406
    },
    "77b2280f-3466-4a77-af12-95caa3e3b0a8": {
      "width": 101.56594848632812,
      "height": 46.888893127441406
    },
    "c3df1858-7d39-4493-9b1f-1c111b9e7d32": {
      "width": 96.5659408569336,
      "height": 43.888893127441406
    },
    "14b68359-3340-4a94-8002-d71057842b8f": {
      "width": 99.56594848632812,
      "height": 40.222206115722656
    },
    "568ffae5-c634-4d21-9651-f7e1862bdf5a": {
      "width": 98.5659408569336,
      "height": 45.388893127441406
    },
    "890383d8-6d81-4210-b5e5-b336b6e80df2": {
      "width": 95.5659408569336,
      "height": 38.888893127441406
    },
    "13d66f7a-95b1-42ac-b75e-cd5b59b2254e": {
      "width": 96.5659408569336,
      "height": 51.888893127441406
    },
    "cd39c513-36dc-4105-9474-dfbd5827381a": {
      "width": 97.5659408569336,
      "height": 40.888893127441406
    },
    "765d9a1c-8837-4c0a-9f73-802d7f042a60": {
      "width": 94.5659408569336,
      "height": 42.888893127441406
    },
    "ee3a9136-0c41-40f6-99a6-df1c3e3085d5": {
      "width": 97.34371948242188,
      "height": 43.222206115722656
    },
    "31fd0199-0c96-44e2-bb84-1dd5ddaf89b8": {
      "width": 126.609375,
      "height": 80.84375
    },
    "9d2b7294-d011-44bd-8f05-00b2a2f5ea9c": {
      "vertices": [
        {
          "x": 1052,
          "y": 532
        }
      ]
    },
    "d68a95b2-c33b-421d-a321-ed104c15fb82": {
      "vertices": [
        {
          "x": 940,
          "y": 250
        }
      ]
    },
    "778768c7-3a3a-4587-b441-7af7fdbdbe7c": {
      "vertices": [
        {
          "x": 871,
          "y": 504
        }
      ]
    },
    "08caaa35-7d5d-4e5a-a078-3ae1e5849066": {
      "vertices": [
        {
          "x": 957,
          "y": 209
        }
      ]
    },
    "603fa46a-6d3d-47d9-bb99-ddd533a07571": {
      "vertices": [
        {
          "x": 978,
          "y": 510
        }
      ]
    },
    "21c9b5a7-808a-492a-8ac6-861b0e6dbdf1": {
      "vertices": [
        {
          "x": 454,
          "y": 518
        }
      ]
    },
    "28df3037-8cc7-46b8-b96a-ce2f49998d2f": {
      "vertices": [
        {
          "x": 657,
          "y": 742
        }
      ]
    },
    "4c003ef4-85d8-42df-95f3-5a952c0667c8": {
      "collapsed": true
    },
    "caf6a775-c3d3-4bd3-986b-af2292821f6e": {
      "collapsed": true
    },
    "20c95a3d-78e5-4772-acf9-1775dfa4b9fb": {
      "collapsed": true
    }
  },
  "tool": "pistar.2.1.0",
  "istar": "2.0",
  "saveDate": "Fri, 28 Jan 2022 16:56:34 GMT",
  "diagram": {
    "width": 3027,
    "height": 2396,
    "name": "SalaZ",
    "customProperties": {
      "Description": ""
    }
  }
}